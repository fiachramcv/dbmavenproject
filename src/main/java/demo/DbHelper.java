package demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DbHelper {
	private static String connectURL = "jdbc:mysql://localhost:3306/equities?useSSL=false";
	private Connection con;
	private static Logger log = LoggerFactory.getLogger(Main.class);

	public DbHelper() throws SQLException {
		this.con = DriverManager.getConnection(connectURL, "root", "password");
	}

	public List<String> getEpicCodes() throws SQLException {	
		try (PreparedStatement pst = con.prepareStatement("Select EPIC from stocks")) {
			ResultSet rs = pst.executeQuery();
			List<String> epics = new ArrayList<>();
			while (rs.next()) {
				String epic = rs.getString("EPIC");
				epics.add(epic);
			}
			return epics;
		}	
	}
	
	public void close() {
		try {
			con.close();
		} catch (SQLException e) {
			log.warn("Failed to close {}", e.toString());
		}
	}
}
