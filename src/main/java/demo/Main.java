package demo;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

	private static Logger log = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) {
		log.info("Connected to DB");
		DbHelper DbH;
		try {
			DbH = new DbHelper();
			List<String> epics = DbH.getEpicCodes();
			log.info(epics.toString());
			DbH.close();
			DbH.getEpicCodes();
		} catch (SQLException e) {
			log.error(e.toString());
		}
	}
	


}
